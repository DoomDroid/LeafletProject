export function routerConfig ($stateProvider, $urlRouterProvider) {
  'ngInject';
  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'app/main/main.html',
      controller: 'MainController',
      controllerAs: 'main'
    })
    .state("map", {
    url: "/map",
    component: "MapComponent",
    template: "<map-comp></map-comp>",

  });

  $urlRouterProvider.otherwise('/');
}
