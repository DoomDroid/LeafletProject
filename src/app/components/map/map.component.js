const SERVICE = new WeakMap();

export const MapComponent = {
  templateUrl: "app/components/map/map.component.html",
  bindings: {mapAtr: "<"},
  controllerAs: "mapCtr",
  controller: class MapController {
    constructor(leafletData) {
      "ngInject";
      // SERVICE.set(this, SharesEmittersService); //private
      this.leafletData = leafletData;
      this.data = {lat: 51.505, lng: -0.09, zoom: 4};
      this.controls = {draw: {}};
      this.chicago = {
        lat: 41.85,
        lng: -87.65,
        zoom: 8
      };
      this.mymarkers = [];

      this.markers = {
        m1: {
          lat: 41.85,
          lng: -87.65,
          message: "I'm a static marker with defaultIcon",
          focus: false,
          icon: {},
        },
      };
      this.defaultIcon = {};

      this.leafIcon = {
        iconUrl: './assets/images/leaf-green.png',
        shadowUrl: './assets/images/leaf-shadow.png',
        iconSize: [38, 95], // size of the icon
        shadowSize: [50, 64], // size of the shadow
        iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
        shadowAnchor: [4, 62],  // the same for the shadow
        popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
      };
      this.orangeLeafIcon = {
        iconUrl: 'assets/images/leaf-orange.png',
        shadowUrl: 'assets/images/leaf-shadow.png',
        iconSize: [38, 95],
        shadowSize: [50, 64],
        iconAnchor: [22, 94],
        shadowAnchor: [4, 62],
        popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
      };
      this.awesomeMarkerIcon = {
        type: 'awesomeMarker',
        icon: 'tag',
        markerColor: 'red'
      };
      this.extraMarkerIcon = {
        type: 'extraMarker',
        icon: 'fa-star',
        markerColor: '#f00',
        prefix: 'fa',
        shape: 'circle'
      };
      // this.layers = {
      //   baselayers: {
      //     mapbox_light: {
      //       name: 'Mapbox Light',
      //       url: 'http://api.tiles.mapbox.com/v4/{mapid}/{z}/{x}/{y}.png?access_token={apikey}',
      //       type: 'xyz',
      //       layerOptions: {
      //         apikey: 'pk.eyJ1IjoiYnVmYW51dm9scyIsImEiOiJLSURpX0pnIn0.2_9NrLz1U9bpwMQBhVk97Q',
      //         mapid: 'bufanuvols.lia22g09'
      //       },
      //       layerParams: {
      //         showOnSelector: false
      //       }
      //     }
      //   },
      //   overlays: {
      //     draw: {
      //       name: 'draw',
      //       type: 'group',
      //       visible: true,
      //       layerParams: {
      //         showOnSelector: false
      //       }
      //     }
      //   }
      // }
      // this.defaults =  {
      //   tileLayer: "http://{s}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png",
      //   zoomControlPosition: "topright",
      //   tileLayerOptions: {
      //     opacity: 0.9,
      //     detectRetina: true,
      //     reuseTiles: true,
      //   },
      //   scrollWheelZoom: false
      // };
    }

    $onInit() {
      // this.drawLayer();
      console.log("it is data", this);
      this.onEventClick();
    }
    onEventClick(){
      this.$on("leafletDirectiveMap.click", function(event, args) {
        let leafEvent = args.leafletEvent;
        this.mymarkers.push({
          lat: leafEvent.latlng.lat,
          lng: leafEvent.latlng.lng,
          message: "My Added Marker"
        });
      });
    }

    // drawLayer() {
    //   this.leafletData.getMap().then(function (map) {
    //     this.leafletData.getLayers().then(function (baselayers) {
    //       let drawnItems = baselayers.overlays.draw;
    //       map.on('draw:created', function (e) {
    //         let layer = e.layer;
    //         drawnItems.addLayer(layer);
    //         console.log(JSON.stringify(layer.toGeoJSON()));
    //       });
    //     });
    //   });
    // }


    // getData() {
    //   SERVICE.get(this).getData()
    //     .then(result => {
    //
    //     })
    // }


  }

};
