/* global malarkey:false, moment:false */

import { config } from "./index.config";
import { routerConfig } from "./index.route";
import { runBlock } from "./index.run";
import { MainController } from "./main/main.controller";
import { WebDevTecService } from "../app/components/webDevTec/webDevTec.service";
import { NavbarDirective } from "../app/components/navbar/navbar.directive";
import { MapComponent } from "../app/components/map/map.component";

angular.module("leafletProject", ["ngAnimate", "ngCookies", "ngTouch", "ngSanitize",
  "ngMessages", "ngAria", "restangular", "ui.router", "ngMaterial", "toastr", "leaflet-directive"])
  .constant("moment", moment)
  .config(config)
  .config(routerConfig)
  .run(runBlock)
  .service("webDevTec", WebDevTecService)
  .controller("MainController", MainController)
  .directive("acmeNavbar", NavbarDirective)
  .component("mapComp", MapComponent);
